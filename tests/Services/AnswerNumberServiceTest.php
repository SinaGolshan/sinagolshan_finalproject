<?php

namespace App\Tests\Services;

use App\Constants\Role;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\User;
use App\Services\AnswerNumberService;
use PHPUnit\Framework\TestCase;

class AnswerNumberServiceTest extends TestCase
{

    private AnswerNumberService $answerNumberService;


    protected function setUp(): void
    {
        $this->answerNumberService = new AnswerNumberService();
    }

    public function testGetNumbers()
    {
        $user = new User();
        $user->setUsername("Brad");
        $user->setRoles(array(Role::$USER));

        $user2 = new User();
        $user2->setUsername("Rob");
        $user2->setRoles(array(Role::$CONSULTER));

        $user3 = new User();
        $user3->setUsername("Tom");
        $user3->setRoles(array(Role::$CONSULTER));

        $question = new Question();
        $question->setUser($user);

        for ($x = 1; $x <= 5; $x++) {
            $answer = new Answer();
            $answer->setUser($user2);
            $question->addAnswer($answer);
        }
        $answer = new Answer();
        $answer->setUser($user3);
        $question->addAnswer($answer);

        $res = $this->answerNumberService->getNumbers($question);

        $this->assertEquals(6, $res["count"]);
        $this->assertEquals(2, $res["consulters"]);


    }
}
