/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';


function getLocale(){
    let uri = window.location.pathname
    return uri.substring(0, uri.indexOf("/", 1))
}

function getAfterLocale(){
    let uri = window.location.pathname
    return uri.substring(uri.indexOf(getLocale()) + getLocale().length)
}
function redirectToLogin() {
    location.replace(getLocale() + "/login")
}

document.getElementById("sign-in-btn").addEventListener("click", redirectToLogin);

function redirectToRegister() {
    location.replace(getLocale() + "/register")
}

document.getElementById("register-btn").addEventListener("click", redirectToRegister);

function setLocationLocale(lang) {
    location.replace("/" + lang + getAfterLocale())
}

function setLocale() {
    let lang = document.getElementById("language").value;
    if (lang === "na")
        lang = "en"
    location.replace("/" + lang + getAfterLocale())
}

document.getElementById("language").addEventListener("change", setLocale);
