<?php

namespace App\Services;

use App\Entity\Question;
use App\Entity\User;

class AnswerNumberService
{
    public function getNumbers(Question $question): array
    {

        $consulters = array();
        foreach ($question->getAnswers() as $answer) {
            $username = $answer->getUser()->getUsername();
            if(!in_array($username, $consulters)){
                $consulters[] = $username;
            }
        }
        return array("count" => sizeof($question->getAnswers()), "consulters" => sizeof($consulters));
    }
}