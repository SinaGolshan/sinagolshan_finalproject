<?php

namespace App\Constants;

class Role
{
    public static string $ADMIN = "ROLE_ADMIN";
    public static string $USER = "ROLE_USER";
    public static string $CONSULTER = "ROLE_CONSULTER";

}