<?php

namespace App\Controller;

use App\Constants\Role;
use App\Entity\Answer;
use App\Entity\Question;
use App\Form\AnswerType;
use App\Repository\AnswerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/{_locale}/answer')]
class AnswerController extends AbstractController
{
    #[Route('/', name: 'app_answer_index', methods: ['GET'])]
    public function index(AnswerRepository $answerRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        return $this->render('answer/index.html.twig', [
            'answers' => $answerRepository->findAll(),
        ]);
    }

    #[Route('/my-list', name: 'app_answer_user_index', methods: ['GET'])]
    public function userIndex(AnswerRepository $answerRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        $answers =  $answerRepository->findByExampleField($user);
        return $this->render('answer/user_index.html.twig', [
            'answers' => $answers,
        ]);
    }

    #[Route('/new/{id}', name: 'app_answer_new', methods: ['GET', 'POST'])]
    public function new(Request $request, Question $question, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        $answer = new Answer();
        $answer->setQuestion($question);
        $answer->setUser($this->getUser());
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($answer);
            $entityManager->flush();

            return $this->redirectToRoute('app_question_show', ["id"=>$question->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('answer/new.html.twig', [
            'answer' => $answer,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_answer_show', methods: ['GET'])]
    #[IsGranted('view', 'answer')]
    public function show(Answer $answer): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        return $this->render('answer/show.html.twig', [
            'answer' => $answer,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_answer_edit', methods: ['GET', 'POST'])]
    #[IsGranted('edit', 'answer')]
    public function edit(Request $request, Answer $answer, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_answer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('answer/edit.html.twig', [
            'answer' => $answer,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_answer_delete', methods: ['POST'])]
    #[IsGranted('delete', 'answer')]
    public function delete(Request $request, Answer $answer, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!in_array(Role::$ADMIN, $user->getRoles()) and !in_array(Role::$CONSULTER, $user->getRoles()))
            return new Response("Yoy are not allowed!", Response::HTTP_FORBIDDEN);

        if ($this->isCsrfTokenValid('delete'.$answer->getId(), $request->request->get('_token'))) {
            $entityManager->remove($answer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_answer_index', [], Response::HTTP_SEE_OTHER);
    }
}
