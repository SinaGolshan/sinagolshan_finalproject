<?php

namespace App\Controller;

use App\Constants\Role;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/{_locale}/home', name: 'app_home')]
    public function home(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $locale = $request->getLocale();

        if (in_array(Role::$USER, $user->getRoles())){
            return $this->redirect("/".$locale."/question/");
        }
        elseif (in_array(Role::$CONSULTER, $user->getRoles())){
            return $this->redirect("/".$locale."/question/");
        }
        elseif (in_array(Role::$ADMIN, $user->getRoles())){
            return $this->redirect("/admin");
        }
        throw new AccessDeniedException();
    }
    #[Route('/', name: 'app_root')]
    public function index(Request $request): Response
    {
        $locale = $request->getLocale();
        return $this->redirect("/".$locale."/home");
    }
}
