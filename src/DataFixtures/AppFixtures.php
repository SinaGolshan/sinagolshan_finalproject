<?php

namespace App\DataFixtures;

use App\Constants\Role;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
         $user = new User();
         $user->setUsername("admin");
         $password = $this->hasher->hashPassword($user, 'admin');
         $user->setPassword($password);
         $user->setRoles(array(Role::$ADMIN));
         $manager->persist($user);

         $user2 = new User();
         $user2->setUsername("john");
         $password = $this->hasher->hashPassword($user2, '123');
         $user2->setPassword($password);
         $user2->setRoles(array(Role::$CONSULTER));
         $manager->persist($user2);

         $user3 = new User();
         $user3->setUsername("tom");
         $password = $this->hasher->hashPassword($user3, '123');
         $user3->setPassword($password);
         $user3->setRoles(array(Role::$USER));
         $manager->persist($user3);

         $user4 = new User();
         $user4->setUsername("joe");
         $password = $this->hasher->hashPassword($user4, '123456');
         $user4->setPassword($password);
         $user4->setRoles(array(Role::$CONSULTER));
         $manager->persist($user4);

         $user5 = new User();
         $user5->setUsername("tim");
         $password = $this->hasher->hashPassword($user5, '123456');
         $user5->setPassword($password);
         $user5->setRoles(array(Role::$USER));
         $manager->persist($user5);

         $question1 = new Question();
         $question1->setTitle("T1");
         $question1->setText("Some test Text to ask");
//         $question1->setCreatedAt(new \DateTimeImmutable());
//         $question1->setUpdatedAt(new \DateTimeImmutable());
         $question1->setUser($user3);
         $manager->persist($question1);

         $question2 = new Question();
         $question2->setTitle("T2");
         $question2->setText("Some test Text to ask 2");
//         $question2->setCreatedAt(new \DateTimeImmutable());
//         $question2->setUpdatedAt(new \DateTimeImmutable());
         $question2->setUser($user5);
         $manager->persist($question2);

         $answer1 = new Answer();
         $answer1->setText("Some test Text to answer");
//         $answer1->setCreatedAt(new \DateTimeImmutable());
//         $answer1->setUpdatedAt(new \DateTimeImmutable());
         $answer1->setUser($user2);
         $answer1->setQuestion($question1);
         $manager->persist($answer1);

         $answer2 = new Answer();
         $answer2->setText("Some test Text to answer 2");
//         $answer2->setCreatedAt(new \DateTimeImmutable());
//         $answer2->setUpdatedAt(new \DateTimeImmutable());
         $answer2->setUser($user4);
         $answer2->setQuestion($question2);
         $manager->persist($answer2);

        $manager->flush();
    }
}
